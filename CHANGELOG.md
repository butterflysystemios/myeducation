# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [Unreleased]
### Added:
- Added file logging support
- Added Bugsee integration
### Changed:
- Replaced globe placeholder image with course image assets
## [Build 5] - 2019-04-01
### Changed
- Uses an activity indicator view for videos that are currently being downloaded.
- Updates the insertion of Course listings, Episode Listings, Images and Videos to backend provided by mac utility app.
- Updates to sync arch.
## [Build 3] - 2018-11-14
### Changed
- Replaces remaining webservice calls with authenticated AWS transfer utility calls for episodes and listing data (in preparation for changes to restrict public permissions of relevant AWS buckets)
## [Build 2] - 2018-10-03
## Fixed
- Adds a Reachability to the video file sync.
- Updates AWS file fetch error handling when fetch is attempted to perform while offline (previous diff would remove)
## [Build 8] - 2018-10-02
### Fixed
- An issue in iOS 12 during launch where cover image data for a course may not yet be available by the time the cover flow attempts to load it.
### Added
- Changes to layout constraint 
- New content import

## [Build 2] - 2018-06-26
### Fixed
- An issue where the episode listings was retrieved from the cache so remote updates may not be reflected immediately.
## [Build 1] - 2018-06-25
### Added
- Supports client deletion of episodes that have been stored locally but not contained in the episode listing file.
## [Build 4] - 2018-04-24
### Added
- Support for iPod touch size classes
- New courses are now added and/or updated when the application starts up.
- A new course listing for MySupply
- Adds a queue based system for course listings

### Changed
- Uses Operation-based API for fetching episodes

## [Build 1] - 2018-03-15
### Added
- Beta badge for testflight releases.
### Deprecated
- Test targets removed from build projects.
## [Build 4] - 2018-03-15
### Added
- App can be launched with a newly added URL scheme.

[Unreleased]: https://bitbucket.org/butterflysystemios/myeducation.git/compare/master..HEAD
