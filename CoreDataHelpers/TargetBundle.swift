//
//  TargetBundle.swift
//  CoreDataHelpers
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation

public class TargetBundle {
    public static func callIn() {
        print("Called into Core Data Helpers")
    }
}
