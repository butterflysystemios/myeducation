//
//  NSFetchRequest+Extensions.swift
//  Moody
//
//  Created by Ukko on 15/11/17.
//  Copyright © 2017 bsys. All rights reserved.
//

import CoreData

extension NSFetchRequest {
    @objc convenience init(entity: NSEntityDescription, predicate: NSPredicate? = nil, batchSize: Int = 0) {
        self.init()
        self.entity = entity
        self.predicate = predicate
        self.fetchBatchSize = batchSize
    }
}

