//
//  FetchListingsOperation.swift
//  MyEducation
//
//  Created by eeekari on 24/4/18.
//  Copyright © 2018 ButterflySystems. All rights reserved.
//

import Foundation
import ButterflyOperations
import AWSS3


enum TransferServiceError: Error {
    case noURLAtLocation
}

class FetchListingsOperation: ButterflyOperations.Operation {
    private let transferUtility: AWSS3TransferUtility
    var completionHandler: (([RemoteEpisode], Error?) -> ())?
    var progressHandler: ((AWSS3TransferUtilityDownloadTask, Progress) -> ()) = { task, progress in
        print("Fetch listings download progress: \(String(describing: progress.localizedDescription))")
    }
    
    init(transferUtility: AWSS3TransferUtility) {
        self.transferUtility = transferUtility
        super.init()
    }
    
    override func execute() {
        if isCancelled {
            finish()
            return
        } else {
            fetchListings()
        }
    }
    
    private let episodeListingBucket = "education.largeassets"
    private let episodeListingKey = "episodes/episode_manifest.json"
    
    private func fetchListings() {
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = { [weak self] task, progress in
            guard let `self` = self, let downloadTask = task as? AWSS3TransferUtilityDownloadTask else { return }
            self.progressHandler(downloadTask, progress)
        }
        let downloadURL = URL.temporary
        let task = transferUtility.download(
            to: downloadURL,
            bucket: episodeListingBucket,
            key: episodeListingKey,
            expression: expression,
            completionHandler: { task, url, data, error in
                self.completeWithError(error, url: url)
        })
        task.continueWith { t in
            if let _ = t.result {
                // no-op
            } else {
                self.completeWithError(t.error, url: nil)
            }
            return nil
        }
    }

    
    fileprivate func completeWithError(_ error: Error?, url: URL?) {
        DispatchQueue.main.async {
            if let err = error {
                self.completeWithError(err)
                return
            }
            guard let contentURL = url else {
                self.completeWithError(TransferServiceError.noURLAtLocation)
                return
            }
            do {
                let data = try Data(contentsOf: contentURL)
                let episodes = try JSONDecoder.default.decode([RemoteEpisode].self, from: data)
                self.completeWithError(nil, episodes: episodes)
            } catch {
                self.completeWithError(error)
            }
        }
    }
    
    private func completeWithError(_ error: Error?, episodes: [RemoteEpisode] = []) {
        if let err = error {
            print("Error getting episodes: \(err)")
            completionHandler?([], err)
        } else {
            completionHandler?(episodes, nil)
        }
        finish(withError: error)
    }
}
