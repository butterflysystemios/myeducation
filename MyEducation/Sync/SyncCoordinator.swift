//
//  VideoSyncCoordinator.swift
//  MyEducation
//
//  Created by EK on 4/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers
import MyEducationModel

protocol SyncCoordinatorDelegate: RemoteDelegate {
    
}

final class SyncCoordinator: RemoteDelegate {
    
    func remoteDidUpdateProgress(_ remote: VideoRemote, importDescription: RemoteEpisode, progress: Progress) {
        print("\(importDescription.title) download progress: \(progress.fractionCompleted)")
    }
    
    let viewContext: NSManagedObjectContext
    let syncContext: NSManagedObjectContext
    var observerTokens: [NSObjectProtocol] = [] //< The tokens registered with NotificationCenter
    let syncGroup: DispatchGroup = DispatchGroup()
    var teardownFlag = atomic_flag()
    let remote: VideoRemote
    let reachability = Reachability()!
    var fetchError: Error? = nil
    init(container: NSPersistentContainer, delegate: SyncCoordinatorDelegate?) {
        viewContext = container.viewContext
        syncContext = container.newBackgroundContext()
        syncContext.name = "Video Sync Background Context"
        syncContext.mergePolicy = MyEducationMergePolicy(mode: .remote)
        remote = VideoRemote()
        remote.delegate = self
        
        try? reachability.startNotifier()
        reachability.whenReachable = { [weak self] _ in
            guard let self = `self` else { return }
            if self.fetchError != nil {
                self.fetchLatestRemoteData()
            }
        }
        setup()
    }
    
    fileprivate func setup() {
        self.perform {
            self.setupContexts()
            self.setupApplicationActiveNotifications()
        }
    }
    
    func tearDown() {
        guard !atomic_flag_test_and_set(&teardownFlag) else { return }
        perform {
            self.removeAllObserverTokens()
        }
    }
    
    deinit {
        guard atomic_flag_test_and_set(&teardownFlag) else { fatalError("deinit called without tearDown() being called.") }
        // We must not call tearDown() at this point, because we can not call async code from within deinit.
        // We want to be able to call async code inside tearDown() to make sure things run on the right thread.
    }
}

extension SyncCoordinator: ContextOwner {
    func addObserverToken(_ token: NSObjectProtocol) {
        observerTokens.append(token)
    }
    
    func removeAllObserverTokens() {
        observerTokens.removeAll()
    }
}

extension SyncCoordinator: ChangeProcessorContext {
    
    /// This is the context that the sync coordinator, change processors, and other sync components do work on.
    var context: NSManagedObjectContext {
        return syncContext
    }
    
    /// This switches onto the sync context's queue. If we're already on it, it will simply run the block.
    func perform(_ block: @escaping () -> ()) {
        syncContext.perform(group: syncGroup, block: block)
    }
    
    func perform<A,B>(_ block: @escaping (A,B) -> ()) -> (A,B) -> () {
        return { (a: A, b: B) -> () in
            self.perform {
                block(a, b)
            }
        }
    }
    
    func perform<A,B,C>(_ block: @escaping (A,B,C) -> ()) -> (A,B,C) -> () {
        return { (a: A, b: B, c: C) -> () in
            self.perform {
                block(a, b, c)
            }
        }
    }
}

extension SyncCoordinator: ApplicationActiveStateObserving {
    func applicationDidBecomeActive() {
        fetchLatestRemoteData()
    }
    
    func applicationDidEnterBackground() {
        syncContext.refreshAllObjects()
    }
    
    fileprivate func fetchLatestRemoteData() {
        perform {
            self.fetchCourses {
                self.remote.fetchListings { manifest, error in
                    DispatchQueue.main.async { self.fetchError = error }
                    guard error == nil else { return }
                    // If the manifest is missing an episode that we have locally, we need to delete it.
                    self.deleteEpisodesNotContainedIn(manifest) {
                        // Fetch videos from manifest
                        self.fetchVideosContainedIn(manifest) {
                            DispatchQueue.main.async {
                                self.fetchError = nil
                            }
                        }
                    }
                }
            }
        }
    }
    
    // MARK: - Fetching Courses
    fileprivate func fetchCourses(completion: @escaping () -> ()) {
        perform {
            self.remote.fetchCourses { manifest, error in
                guard error == nil else { return completion() }
                // remove old courses
                self.deleteCoursesNotContainedIn(manifest) {
                    // fetch cover images
                    self.fetchCoverImagesContainedIn(manifest, completion: completion)
                }
            }
        }
    }
    
    fileprivate func fetchCourseCoverImage(for course: RemoteCourse, completion: @escaping () -> ()) {
        perform {
            self.remote.fetchCoverImage(for: course) { data in
                self.perform {
                    let _ = Course.findOrCreateCourse(with: course.category, name: course.name, coverImageData: data, remoteIdentifier: course.remoteIdentifier, createdAt: course.createdAt, lastUpdatedAt: course.updatedAt, in: self.context)
                    completion()
                }
            }
        }
    }
    
    fileprivate func deleteCoursesNotContainedIn(_ manifest: [RemoteCourse], completion: @escaping () -> ()) {
        perform {
            let coursesToLocallyDelete = { () -> [Course] in
                let coursesToDelete = Course.fetch(in: self.context) { request in
                    request.predicate = NSPredicate(format: "NOT %K in %@", argumentArray: [#keyPath(Course.remoteIdentifier), manifest.map { $0.remoteIdentifier } ])
                    request.returnsObjectsAsFaults = false
                }
                return coursesToDelete
            }()
        
            for courseToDelete in coursesToLocallyDelete {
                self.context.delete(courseToDelete)
            }
            try? self.context.save()
            completion()
        }
    }
    
    fileprivate func fetchCoverImagesContainedIn(_ manifest: [RemoteCourse], completion: @escaping () -> ()) {
        perform {
            let group = DispatchGroup()
            for remoteCourse in manifest {
                group.enter()
                self.fetchCourseCoverImage(for: remoteCourse) {
                    group.leave()
                }
            }
            group.notify(queue: DispatchQueue.global(qos: .userInteractive)) {
                self.perform {
                    try? self.context.save()
                    completion()
                }
            }
        }
    }
    
    fileprivate func deleteEpisodesNotContainedIn(_ manifest: [RemoteEpisode], completion: @escaping () -> ()) {
        perform {
            let episodesToLocallyDelete = { () -> [Episode] in
                let episodesToDelete = Episode.fetch(in: self.context) { request in
                    request.predicate = NSPredicate(format: "NOT %K in %@", argumentArray: [#keyPath(Episode.remoteIdentifier), manifest.map { $0.remoteIdentifier }])
                    request.returnsObjectsAsFaults = false
                }
                return episodesToDelete
            }()
            
            for episodeToDelete in episodesToLocallyDelete {
                print("Deleting episode no longer in listing: \(episodeToDelete.remotePath)")
                if let deleteStorage = episodeToDelete.storedAt {
                    do {
                        try FileManager.default.removeItem(at: deleteStorage) // delete the stored location on the filesystem
                        self.context.delete(episodeToDelete)
                    } catch {
                        print("Error deleting the stored location of a deleted episode: \(error)")
                    }
                }
            }
            try? self.context.save()
            completion()
        }
    }
    
    fileprivate func fetchVideosContainedIn(_ manifest: [RemoteEpisode], completion: @escaping () -> ()) {
        perform {
            let group = DispatchGroup()
            for episode in manifest {
                group.enter()
                if let fetchedEpisode = Episode.first(in: self.context, where: NSPredicate(format: "%K == %@", argumentArray: [#keyPath(Episode.remoteIdentifier), episode.remoteIdentifier])) {
                    if fetchedEpisode.videoUpdatedAt < episode.videoUpdatedAt || fetchedEpisode.storedAt == nil {
                        self.importVideo(from: episode, to: fetchedEpisode) {
                            group.leave()
                        }
                    } else {
                        fetchedEpisode.reconfigureEpisode(using: episode, in: self.context)
                        group.leave()
                    }
                } else {
                    let insertedEpisode = Episode.insertOrUpdate(episode, storageLocation: nil, in: self.context)
                    self.importVideo(from: episode, to: insertedEpisode) {
                        group.leave()
                    }
                }
            }
            group.notify(queue: DispatchQueue.main) {
                completion()
            }
        }
    }
    
    fileprivate func importVideo(from remoteEpisode: RemoteEpisode, to fetchedEpisode: Episode, completion: @escaping () -> ()) {
        perform {
            self.remote.fetch(remoteEpisode) { url in
                self.context.perform {
                    if let storageURL = url {
                        fetchedEpisode.saveVideoFileAt(url: storageURL)
                        let result = fetchedEpisode.asset.generateMetaData()
                        fetchedEpisode.thumbnailData = result.thumbnail
                        if let duration = result.duration {
                            fetchedEpisode.duration = Measurement<UnitDuration>(value: duration, unit: .seconds)
                        }
                    }
                    try? self.context.save()
                    completion()
                }
            }
        }
    }
}

extension MyEducationModel.Episode {
    fileprivate func reconfigureEpisode(using episode: RemoteEpisode, in context: NSManagedObjectContext) {
        self.reconfigureEpisodeInfo(
            updatedAt: episode.updatedAt,
            title: episode.title,
            part: episode.part,
            transcript: episode.transcript,
            courseIdentifiers: episode.courseCategories,
            searchTags: episode.additionalTags,
            in: context
        )
    }
    
    @discardableResult fileprivate static func insertOrUpdate(_ episode: RemoteEpisode, storageLocation: URL?, in context: NSManagedObjectContext) -> Episode {
        return Episode.insertOrUpdateEpisode(
            in: context,
            storedAt: storageLocation,
            remoteIdentifier: episode.remoteIdentifier,
            remotePath: episode.remotePath,
            title: episode.title,
            transcript: episode.transcript,
            createdAt: episode.createdAt,
            updatedAt: episode.updatedAt,
            videoUpdatedAt: episode.videoUpdatedAt,
            part: episode.part,
            searchTags: episode.additionalTags,
            courseIdentifiers: episode.courseCategories
        )
    }
}













