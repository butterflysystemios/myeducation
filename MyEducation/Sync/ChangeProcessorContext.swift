//
//  ChangeProcessorContext.swift
//  MyEducation
//
//  Created by EK on 4/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation
import CoreData

protocol ChangeProcessorContext: class {
    var context: NSManagedObjectContext { get }
    
    /// Wraps a block such that it is run on the right queue.
    func perform(_ block: @escaping () -> ())
    
    /// Wraps a block such that it is run on the right queue.
    func perform<A, B>(_ block: @escaping (A, B) -> ()) -> (A, B) -> ()
    
    /// Wraps a block such that it is run on the right queue.
    func perform<A, B, C>(_ block: @escaping (A, B, C) -> ()) -> (A, B, C) -> ()
    
}

extension NSManagedObjectContext {
    func perform(group: DispatchGroup, block: @escaping () -> ()) {
        group.enter()
        perform {
            block()
            group.leave()
        }
    }
    
    fileprivate var changedObjectsCount: Int {
        return insertedObjects.count + updatedObjects.count + deletedObjects.count
    }
    
    func delayedSaveOrRollback(group: DispatchGroup, completion: @escaping (Bool) -> () = { _ in }) {
        let changeCountLimit = 20
        guard changeCountLimit >= changedObjectsCount else { return completion(saveOrRollback()) }
        let queue = DispatchQueue.global(qos: DispatchQoS.QoSClass.default)
        group.notify(queue: queue) {
            self.perform(group: group) {
                guard self.hasChanges else { return completion(true) }
                completion(self.saveOrRollback())
            }
        }
    }
}

