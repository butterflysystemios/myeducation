//
//  FetchCourseCoverImageOperation.swift
//  MyEducation
//
//  Created by eeekari on 1/4/19.
//  Copyright © 2019 ButterflySystems. All rights reserved.
//

import Foundation
import ButterflyOperations
import AWSS3

class FetchCourseCoverImageOperation: ButterflyOperations.Operation, ProgressReporting {
    var progress = Progress()
    var completion: ((Data?) -> ())?
    
    var progressHandler: ((AWSS3TransferUtilityDownloadTask, Progress) -> ()) = { task, progress in
        print("Fetch listings download progress: \(String(describing: progress.localizedDescription))")
    }
    
    private let transferUtility: AWSS3TransferUtility
    private let bucket = "education.largeassets"
    private let imageKey: String
    
    init(transferUtility: AWSS3TransferUtility, course: RemoteCourse) {
        self.transferUtility = transferUtility
        self.imageKey = "courses/assets/\(course.remoteIdentifier.uuidString)"
        super.init()
    }
    
    override func execute() {
        if isCancelled {
            finish()
            return
        } else {
            fetchCourses()
        }
    }
    
    private func fetchCourses() {
        let expression = AWSS3TransferUtilityDownloadExpression()
        expression.progressBlock = { [weak self] task, progress in
            guard let `self` = self, let downloadTask = task as? AWSS3TransferUtilityDownloadTask else { return }
            self.progress = progress
            self.progressHandler(downloadTask, progress)
        }
        let downloadURL = URL.temporary
        let task = transferUtility.download(
            to: downloadURL,
            bucket: bucket,
            key: imageKey,
            expression: expression,
            completionHandler: { [weak self] task, url, data, error in
                self?.completeWithError(error, url: url)
        })
        task.continueWith { [weak self] t in
            if let _ = t.result {
                // no-op
            } else {
                self?.completeWithError(t.error, url: nil)
            }
            return nil
        }
    }
    
    fileprivate func completeWithError(_ error: Error?, url: URL?) {
        DispatchQueue.main.async {
            if let err = error {
                self.completion?(nil)
                self.finishWithError(err as NSError?)
                return
            }
            guard let contentURL = url else {
                let err = TransferServiceError.noURLAtLocation
                self.completion?(nil)
                self.finishWithError(err as NSError?)
                return
            }
            do {
                let data = try Data(contentsOf: contentURL)
                self.completion?(data)
                self.finishWithError(nil)
            } catch let err {
                self.completion?(nil)
                self.finishWithError(err as NSError?)
            }
        }
    }
}
