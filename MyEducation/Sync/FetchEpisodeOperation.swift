//
//  FetchEpisodeOperation.swift
//  MyEducation
//
//  Created by eeekari on 24/4/18.
//  Copyright © 2018 ButterflySystems. All rights reserved.
//

import Foundation
import ButterflyOperations
import AWSS3

class FetchEpisodeOperation: ButterflyOperations.Operation {
    var progressHandler: ((AWSS3TransferUtilityDownloadTask, Progress) -> ())?
    var completionHandler: ((RemoteEpisode, URL?) -> ())?
    
    private let transferUtility: AWSS3TransferUtility
    private let episode: RemoteEpisode
    
    init(episode: RemoteEpisode, transferUtility: AWSS3TransferUtility = AWSS3TransferUtility.default()) {
        self.episode = episode
        self.transferUtility = transferUtility
        super.init()
    }
    
    override func execute() {
        if isCancelled {
            finish()
            return
        } else {
            fetchEpisode()
        }
    }
    
    let expression = AWSS3TransferUtilityDownloadExpression()
    
    fileprivate func fetchEpisode() {
        expression.progressBlock = { [weak self] task, progress in
            guard let `self` = self, let downloadTask = task as? AWSS3TransferUtilityDownloadTask else { return }
                self.progressHandler?(downloadTask, progress)
        }
        
        let downloadURL = URL.group.appendingPathComponent(episode.remotePath)
        let task = transferUtility.download(
            to: downloadURL,
            bucket: "education.largeassets",
            key: "episodes/assets/\(episode.remotePath)",
            expression: expression,
            completionHandler: { task, url, data, error in
                self.completeWithError(error, url: url)
        })
        task.continueWith { t in
            if let _ = t.result {
                // no-op
            } else {
                self.completeWithError(t.error, url: nil)
            }
            return nil
        }
    }

    fileprivate func completeWithError(_ error: Error?, url: URL?) {
        DispatchQueue.main.async {
            if let err = error {
                self.completionHandler?(self.episode, nil)
                self.finish(withError: err)
                return
            }
            guard let url = url else {
                print("Errored because URL was not returned as part of the amazon task")
                self.completionHandler?(self.episode, nil)
                self.finish(withError: TransferServiceError.noURLAtLocation)
                return
            }
            let newURL = URL.group.appendingPathComponent(UUID().uuidString.appending(self.episode.remotePath))
            do {
                try FileManager.default.moveItem(at: url, to: newURL)
                self.completionHandler?(self.episode, newURL)
                self.finish()
            } catch {
                print("Error moving file!: \(error)")
                self.completionHandler?(self.episode, nil)
                self.finish(withError: error)
            }
        }
    }
}
