//
//  VideoDownloader.swift
//  MyEducation
//
//  Created by EK on 4/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation
import MyEducationModel
import AWSS3
import AVFoundation
import ButterflyOperations

protocol RemoteDelegate: class {
    func remoteDidUpdateProgress(_ remote: VideoRemote, importDescription: RemoteEpisode, progress: Progress)
}

final class VideoRemote {
    
    // MARK: - Public
    
    weak var delegate: RemoteDelegate?
    
    
    func fetchCourses(completion: @escaping ([RemoteCourse], Error?) -> ()) {
        let fetchCoursesOperation = FetchCoursesOperation(transferUtility: self.transferUtility)
        fetchCoursesOperation.completionHandler = completion
        operationQueue.addOperation(fetchCoursesOperation)
    }
    
    func fetchCoverImage(for course: RemoteCourse, completion: @escaping (Data?) -> ()) {
        let fetchCoverImageOperation = FetchCourseCoverImageOperation(transferUtility: self.transferUtility, course: course)
        fetchCoverImageOperation.completion = completion
        operationQueue.addOperation(fetchCoverImageOperation)
    }
    
    func fetchListings(completion: @escaping ([RemoteEpisode], Error?) -> ()) {
        let fetchListingsOperation = FetchListingsOperation(transferUtility: self.transferUtility)
        fetchListingsOperation.completionHandler = completion
        operationQueue.addOperation(fetchListingsOperation)
    }
    
    func fetch(_ episode: RemoteEpisode, completion: @escaping (URL?) -> ()) {
        let listPathKey = "episodes/assets/\(episode.remotePath)"
        if let importDescription = listingsByListPathKeys[listPathKey] {
            // a task already exists for this...
            print("Import description for \(importDescription) is already enqueued or being downloaded")
            completion(nil)
            return
        }
        
        listingsByListPathKeys[listPathKey] = episode // add the task into the progress dictionary
        
        let fetchOperation = FetchEpisodeOperation(episode: episode, transferUtility: self.transferUtility)
        fetchOperation.completionHandler = { episode, url in
            self.listingsByListPathKeys[listPathKey] = nil // remove the task from the progress dictionary
            completion(url)
        }
        fetchOperation.progressHandler = { [weak self] task, progress in
            guard let `self` = self else { return }
            self.updateDownloadProgressForTask(task, progress: progress)
        }
        operationQueue.addOperation(fetchOperation)
    }
    
    // MARK: - Private
    private let operationQueue: ButterflyOperations.OperationQueue = {
        let queue = ButterflyOperations.OperationQueue()
        return queue
    }()
    
    private let transferUtility = AWSS3TransferUtility.default()
    private let queue = DispatchQueue(label: "VideoRemoteListingsQueue")
    private var _listingsByListPathKeys: [String : RemoteEpisode] = [:]
    private var listingsByListPathKeys: [String : RemoteEpisode] {
        get { return queue.sync { _listingsByListPathKeys } }
        set { queue.sync { _listingsByListPathKeys = newValue } }
    }
    
    private func updateDownloadProgressForTask(_ task: AWSS3TransferUtilityDownloadTask, progress: Progress) {
        if let des = listingsByListPathKeys[task.key] {
            delegate?.remoteDidUpdateProgress(self, importDescription: des, progress: progress)
        }
    }
    

}

extension URL {
    fileprivate static func tempFile(listing path: String) -> URL {
        return URL(fileURLWithPath: NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(path + UUID().uuidString)
    }
}

