//
//  ApplicationActiveStateObserving.swift
//  MyEducation
//
//  Created by EK on 4/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation
import CoreData
import CoreDataHelpers

protocol ObserverTokenStore: class {
    func addObserverToken(_ token: NSObjectProtocol)
}

protocol ApplicationActiveStateObserving: ObserverTokenStore {
    func perform(_ block: @escaping () -> ())
    func applicationDidBecomeActive()
    func applicationDidEnterBackground()
}

protocol ContextOwner: ObserverTokenStore {
    var viewContext: NSManagedObjectContext { get }
    var syncContext: NSManagedObjectContext { get }
    var syncGroup: DispatchGroup { get }
}

extension ContextOwner {
    func setupContexts() {
        setupContextNotificationObserving()
    }
    
    func setupContextNotificationObserving() {
        addObserverToken(
            viewContext.addContextDidSaveNotificationObserver { [weak self] note in
                self?.viewContextDidSave(note)
            }
        )
        addObserverToken(
            syncContext.addContextDidSaveNotificationObserver { [weak self] note in
                self?.syncContextDidSave(note)
            }
        )
    }
    
    /// Merge changes from view -> sync context.
    fileprivate func viewContextDidSave(_ note: ContextDidSaveNotification) {
        syncContext.performMergeChanges(from: note)
    }
    
    /// Merge changes from sync -> view context.
    fileprivate func syncContextDidSave(_ note: ContextDidSaveNotification) {
        viewContext.performMergeChanges(from: note)
    }
}


extension ApplicationActiveStateObserving {
    func setupApplicationActiveNotifications() {
        let center = NotificationCenter.default
        addObserverToken(center.addObserver(forName: .UIApplicationDidEnterBackground, object: nil, queue: nil) { [weak self] note in
            guard let observer = self else { return }
            observer.perform {
                observer.applicationDidEnterBackground()
            }
        })
        addObserverToken(center.addObserver(forName: .UIApplicationDidBecomeActive, object: nil, queue: nil) { [weak self] note in
            guard let observer = self else { return }
            observer.perform {
                observer.applicationDidBecomeActive()
            }
        })
        DispatchQueue.main.async {
            if UIApplication.shared.applicationState == .active {
                self.perform {
                    self.applicationDidBecomeActive()
                }
            }
        }
    }
}
