//
//  AppDelegate.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreDataHelpers
import MyEducationModel
import CoreData
import AWSCore
import AWSSES
import AWSS3
import AVKit
import PDFKit
import Bugsee

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    
    var app: App?
    var syncCoordinator: SyncCoordinator?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        FileLog.shared.redirectConsoleToFile()
        Bugsee.launchApplication()
        Bugsee.setEmail("automated.testing@butterfly.systems")
        
        let provider = AWSStaticCredentialsProvider(accessKey: Environment.Amazon.accessKey, secretKey: Environment.Amazon.secretKey)
        let configuration = AWSServiceConfiguration(region: .APSoutheast2, credentialsProvider: provider)
        AWSServiceManager.default()!.defaultServiceConfiguration = configuration
        
        print(Environment.current.description)
        
        let audioSession = AVAudioSession.sharedInstance()
        do { try audioSession.setCategory(AVAudioSessionCategoryPlayback) }
        catch { print("Unable to set audio session category \(#function)") }
        
        MyEducationModel.createSharedContainer { container in
            self.app = App(application, container: container, window: self.window!)
            self.syncCoordinator = SyncCoordinator(container: container, delegate: self.app!)
        }
        return true
    }
    
    func application(_ application: UIApplication, handleEventsForBackgroundURLSession identifier: String, completionHandler: @escaping () -> Void) {
        AWSS3TransferUtility.interceptApplication(application, handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
    }
    
    func applicationDidReceiveMemoryWarning(_ application: UIApplication) {
        app?.receivedMemoryWarning()
    }
}

//MARK: - BugseeDelegate
extension AppDelegate: BugseeDelegate {
    public func bugseeAttachments(for report: BugseeReport) -> [BugseeAttachment] {
        FileLog.shared.flushOutput()
        var attachments = [BugseeAttachment]()
        for logFile in [FileLog.shared.currentLogFile, FileLog.shared.previousLogFile] {
            let url = URL(fileURLWithPath: logFile)
            let filename = url.lastPathComponent
            if let data = try? Data(contentsOf: url), let att = BugseeAttachment(name: "logFile", filename: filename, data: data) {
                attachments.append(att)
            }
        }
        return attachments
    }
}

extension Bugsee {
    @nonobjc static func launchApplication() {
        let options : [String: Any] = [BugseeFrameRateKey : BugseeFrameRateLow.rawValue]
        launch(token: "dfc10780-e516-481c-a58f-e077e333ef19", options: options)
    }
}





