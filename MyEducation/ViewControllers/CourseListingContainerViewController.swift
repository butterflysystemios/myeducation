//
//  CourseListingContainerViewController.swift
//  MyEducation
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreData
import MyEducationModel

final class CourseListingContainerViewController: UIViewController, SegueHandler {
    var context: NSManagedObjectContext!
    
    var didSelectEpisode: (Episode) -> () = { _ in }
    
    private func didSelectCourse(_ course: Course?) {
        if let c = course {
            episodesFlowController?.source = EpisodeSource.course(c)
        } else {
            episodesFlowController?.source = EpisodeSource.all
        }
    }
    
    enum SegueIdentifier: String {
        case embedCoverFlow
        case embedEpisodesFlow
    }
    
    private var coverFlowController: CoverFlowViewController?
    private var episodesFlowController: EpisodesCollectionViewController?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segueIdentifier(for: segue) {
        case .embedCoverFlow:
            guard let coverFlow = segue.destination as? CoverFlowViewController else {
                fatalError("Expected CoverFlowViewController")
            }
            coverFlow.context = context
            coverFlow.didSelect = { [unowned self] in self.didSelectCourse($0) }
            coverFlowController = coverFlow
        case .embedEpisodesFlow:
            guard let episodesFlow = segue.destination as? EpisodesCollectionViewController else {
                fatalError("Expected EpisodesCollectionViewController")
            }
            episodesFlow.context = context

            episodesFlow.didSelect = { [unowned self] in self.didSelectEpisode($0) }
            episodesFlowController = episodesFlow
            let result = Course.fetch(in: self.context) {
                $0.fetchLimit = 1
                $0.sortDescriptors = Course.defaultSortDescriptors
            }.first
            didSelectCourse(result)
        }
    }
}
