//
//  EpisodesCollectionViewController.swift
//  MyEducation
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreData
import CoreDataHelpers
import MyEducationModel

final class EpisodesCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var source: EpisodeSource = .all {
        didSet {
            updateDataSource()
        }
    }
    
    var context: NSManagedObjectContext!
    var didSelect: (Episode) -> () = { _ in }
    var dataSource: CollectionViewDataSource<EpisodesCollectionViewController>!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView?.delegate = self
        //(collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.minimumLineSpacing = 1.0
        setupDataSource()
    }
    
    private func setupDataSource() {
        let request = Episode.sortedFetchRequest(with: source.predicate)
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        dataSource = CollectionViewDataSource(collectionView: collectionView!, cellIdentifier: "EpisodeCell", fetchedResultsController: frc, delegate: self)
    }
    
    private func updateDataSource() {
        dataSource?.reconfigureFetchRequest { request in
            request.predicate = source.predicate
        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let ep = dataSource.item(at: indexPath)
        didSelect(ep)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        collectionViewLayout.invalidateLayout()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var size = CGSize.zero
        if let layout = collectionViewLayout as? UICollectionViewFlowLayout {
            size = layout.itemSize
            size.width = collectionView.bounds.width - 15
        }
        return size
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 0)
    }
}

extension EpisodesCollectionViewController: CollectionViewDataSourceDelegate {
    func configure(_ cell: EpisodeCollectionViewCell, for object: Episode) {
        cell.nameLabel.text = object.title
        if object.storedAt == nil {
            cell.activityIndicator.startAnimating()
            cell.durationLabel.text = nil
        } else {
            cell.activityIndicator.stopAnimating()
            cell.durationLabel.text = durationFormatter.string(from: object.duration)
        }
    }
}

fileprivate let durationFormatter: MeasurementFormatter = {
    let formatter = MeasurementFormatter()
    formatter.unitOptions = [.naturalScale]
    formatter.numberFormatter.maximumFractionDigits = 0
    return formatter
}()

