//
//  CoverFlowCollectionViewController.swift
//  MyEducation
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreData
import MyEducationModel
import CoreDataHelpers

final class CoverFlowViewController: UIViewController {
    var didSelect: (Course) -> () = { _ in }
    var context: NSManagedObjectContext!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var layout: CoverFlowCollectionViewLayout!
    
    
    // MARK: - ViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCollectionView()
        setupDataSource()
    }
    
    private var settingsController: CoverFlowSettingsViewController?
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let dvc = segue.destination as? CoverFlowSettingsViewController {
            settingsController = dvc
            settingsController?.delegate = self
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        layout.invalidateLayout()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        layout.itemSize = CGSize(
            width: collectionView.bounds.size.width * originalItemSize.width / originalCollectionViewSize.width,
            height: collectionView.bounds.size.height * originalItemSize.height / originalCollectionViewSize.height
        )
        
        updateLayoutConfiguration()
        collectionView.setNeedsLayout()
        collectionView.layoutIfNeeded()
        collectionView.reloadData()
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            getCurrentRow()
        }
    }
    
    // MARK: - ViewConfiguration
    
    private var originalItemSize: CGSize = .zero
    private var originalCollectionViewSize: CGSize = .zero
    
    private func setupCollectionView() {
        collectionView.delegate = self
        originalItemSize = layout.itemSize
        originalCollectionViewSize = collectionView.bounds.size
        collectionView.backgroundColor = UIColor.black
    }
    
    
    private func updateLayoutConfiguration() {
        // Setting some defaults
        layout.maxCoverDegree = 45
        layout.coverDensity = 0.06
        layout.minCoverScale = 0.69
        layout.minCoverOpacity = 0.60
        settingsController?.refreshView()
    }
    
    fileprivate func getCurrentRow() {
        let width = collectionView.collectionViewLayout.collectionViewContentSize.width / CGFloat(collectionView.numberOfItems(inSection: 0))
        let currentRow = collectionView.contentOffset.x / width
        print("Currently displaying item: \(currentRow)")
    }
    
    // MARK: - DataSource
    private var dataSource: CollectionViewDataSource<CoverFlowViewController>!
    
    fileprivate func setupDataSource() {
        let request = Course.sortedFetchRequest
        let frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
        dataSource = CollectionViewDataSource(collectionView: collectionView, cellIdentifier: "AppCourseCollectionViewCell", fetchedResultsController: frc, delegate: self)
    }
    
}

extension CoverFlowViewController: CollectionViewDataSourceDelegate, CoverFlowSettingsViewControllerDelegate, UICollectionViewDelegate {

    func configure(_ cell: AppCourseCollectionViewCell, for object: Course) {
        print("Configuring cell for course: \(object.name)")
        if let imageData = object.coverImageData, let imageFromData = UIImage(data: imageData) {
            cell.coverImageView.image = imageFromData
        } else {
            cell.coverImageView.image =  UIImage(named: "CourseID_\(object.courseID)") ?? UIImage(named: "world")
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        let width = collectionView.collectionViewLayout.collectionViewContentSize.width / CGFloat(collectionView.numberOfItems(inSection: indexPath.section))
        let resultingOffset = (width * CGFloat(indexPath.row))
        collectionView.setContentOffset(CGPoint(x: resultingOffset, y: 0), animated: true)
        didSelect(dataSource.item(at: indexPath))
    }
    
    func settingsChanged(_ sender: CoverFlowSettingsViewController, sliderChange: SliderChange) {
        switch sliderChange {
        case .degrees(let value):
            layout.maxCoverDegree = value
        case .density(let value):
            layout.coverDensity = value
        case .opacity(let value):
            layout.minCoverOpacity = value
        case .scale(let value):
            layout.minCoverScale = value
        }
        collectionView.reloadData()
    }
    
    func valueForSettingSlider(_ sender: CoverFlowSettingsViewController, kind: Slider) -> SliderChange {
        switch kind {
        case .degrees:
            return SliderChange(kind, value: layout.maxCoverDegree)
        case .density:
            return SliderChange(kind, value: layout.coverDensity)
        case .opacity:
            return SliderChange(kind, value: layout.minCoverOpacity)
        case .scale:
            return SliderChange(kind, value: layout.minCoverScale)
        }
    }
}




