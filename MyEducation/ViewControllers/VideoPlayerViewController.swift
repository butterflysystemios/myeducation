//
//  VideoPlayerViewController.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import AVFoundation
import AVKit
import UIKit
import MyEducationModel
import CoreDataHelpers


public extension AVPlayerItem {
    static let didPlayToEndTime = NotificationDescriptor<()>(name: .AVPlayerItemDidPlayToEndTime) { _ in () }
}

public final class VideoPlayerViewController: UIViewController {
    var episode: Episode!
    let playerViewController = AVPlayerViewController()
    
    public var thumbnail: UIImage? {
        didSet {
            guard let _ = playerViewController.player?.currentItem else { return }
            let _ = AVMetadataItem.item(identifier: AVMetadataKey.commonKeyArtwork.rawValue, image: thumbnail)
        }
    }
    
    public var didPlayToEnd: (() -> ())?
    private var didPlayToEndTimeToken: ObserverToken?
    
    public init(_ episode: Episode) {
        self.episode = episode
        super.init(nibName: nil, bundle: nil)
        
        let playerItem = AVPlayerItem(url: episode.storedAt!)
        let player = AVPlayer(playerItem: playerItem)
        playerViewController.player = player
        playerViewController.allowsPictureInPicturePlayback = true
        playerViewController.entersFullScreenWhenPlaybackBegins = true
        playerViewController.showsPlaybackControls = true
    }
    
    required public init?(coder decoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func viewDidLoad() {
        super.viewDidLoad()

        playerViewController.delegate = self
        addChildViewController(playerViewController)
        view.addSubview(playerViewController.view)
        playerViewController.didMove(toParentViewController: self)
        playerViewController.view.translatesAutoresizingMaskIntoConstraints = false
        playerViewController.view.topAnchor.constrainEqual(view.topAnchor)
        playerViewController.view.leadingAnchor.constrainEqual(view.leadingAnchor)
        playerViewController.view.trailingAnchor.constrainEqual(view.trailingAnchor)
        playerViewController.view.bottomAnchor.constrainEqual(view.bottomAnchor)
        play()
    }
    
    public override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    public override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let playerItem = playerViewController.player?.currentItem {
            didPlayToEndTimeToken = NotificationCenter.default.addObserver(descriptor: AVPlayerItem.didPlayToEndTime, object: playerItem) { [unowned self] _ in
                self.didPlayToEnd?()
            }
        }
    }
    
    public override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        didPlayToEndTimeToken = nil
    }
    
    public func play() {
        playerViewController.player?.play()
        if AVPictureInPictureController.isPictureInPictureSupported() {
            print("Supported pic in pic")
        } else {
            print("Unsupported pic in pic")
        }
    }
}

extension VideoPlayerViewController: AVPlayerViewControllerDelegate {

    
    public func playerViewControllerShouldAutomaticallyDismissAtPictureInPictureStart(_ playerViewController: AVPlayerViewController) -> Bool {
        return false
    }

    public func playerViewController(_ playerViewController: AVPlayerViewController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        let isPlayer = playerViewController == self.playerViewController
        print("Player is player: \(isPlayer)")
        if !isPlayer, let top = navigationController?.topViewController {
            top.present(playerViewController, animated: true) {
                completionHandler(true)
            }
        } else {
            completionHandler(false)
        }
    }
    
    public func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        print("Failed start")
        print(error)
    }
    
    public func playerViewControllerWillStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("Will start")
    }
    
    public func playerViewControllerDidStopPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("Did stop")
    }
    
    public func playerViewControllerDidStartPictureInPicture(_ playerViewController: AVPlayerViewController) {
        print("Did start")
    }
}

