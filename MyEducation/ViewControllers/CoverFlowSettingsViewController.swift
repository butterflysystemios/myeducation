//
//  CoverFlowSettingsViewController.swift
//  MyEducation
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit

protocol CoverFlowSettingsViewControllerDelegate: class {
    func settingsChanged(_ sender: CoverFlowSettingsViewController, sliderChange: SliderChange)
    func valueForSettingSlider(_ sender: CoverFlowSettingsViewController, kind: Slider) -> SliderChange
}

final class CoverFlowSettingsViewController: UIViewController {
    
    @IBOutlet weak var maxDegreesValueLabel: UILabel!
    @IBOutlet weak var coverDensityValueLabel: UILabel!
    @IBOutlet weak var minOpacityValueLabel: UILabel!
    @IBOutlet weak var minScaleValueLabel: UILabel!
    
    @IBOutlet weak var minScaleSlider: UISlider!
    @IBOutlet weak var maxDegreesSlider: UISlider!
    @IBOutlet weak var coverDensitySlider: UISlider!
    @IBOutlet weak var minOpacitySlider: UISlider!
    
    weak var delegate: CoverFlowSettingsViewControllerDelegate!
    
    @IBAction func degreesSliderValueChanged(_ sender:UISlider) {
        let newMaxCover = CGFloat(sender.value)
        let change = SliderChange.degrees(newMaxCover)
        maxDegreesValueLabel.text = String(format: "%.2f", newMaxCover)
        delegate.settingsChanged(self, sliderChange: change)
    }
    
    @IBAction func densitySliderValueChanged(_ sender:UISlider) {
        let newDensity = CGFloat(sender.value)
        let change = SliderChange.density(newDensity)
        coverDensityValueLabel.text = String(format: "%.2f", newDensity)
        delegate.settingsChanged(self, sliderChange: change)
    }
    
    @IBAction func opacitySliderValueChanged(_ sender:UISlider) {
        let minCoverOpacity = CGFloat(sender.value)
        let change = SliderChange.opacity(minCoverOpacity)
        minOpacityValueLabel.text = String(format: "%.2f", minCoverOpacity)
        delegate.settingsChanged(self, sliderChange: change)
    }
    
    @IBAction func scaleSliderValueChanged(_ sender:UISlider) {
        let minCoverScale = CGFloat(sender.value)
        let change = SliderChange.scale(minCoverScale)
        minScaleValueLabel.text = String(format: "%.2f", minCoverScale)
        delegate.settingsChanged(self, sliderChange: change)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        refreshView()
    }
    
    func refreshView() {
        let maxDeg = delegate.valueForSettingSlider(self, kind: .degrees)
        maxDegreesValueLabel?.text = String(format: "%.2f", maxDeg.value)
        maxDegreesSlider?.value = Float(maxDeg.value)
        
        let coverDensity = delegate.valueForSettingSlider(self, kind: .density)
        coverDensityValueLabel?.text = String(format: "%.2f", coverDensity.value)
        coverDensitySlider?.value = Float(coverDensity.value)
        
        let minOpacity = delegate.valueForSettingSlider(self, kind: .opacity)
        minOpacityValueLabel?.text = String(format: "%.2f", minOpacity.value)
        minOpacitySlider?.value = Float(minOpacity.value)
        
        let minScale = delegate.valueForSettingSlider(self, kind: .scale)
        minScaleValueLabel?.text = String(format: "%.2f", minScale.value)
        minScaleSlider?.value = Float(minScale.value)
    }
}








enum Slider {
    case degrees
    case density
    case opacity
    case scale
}

enum SliderChange {
    case degrees(CGFloat)
    case density(CGFloat)
    case opacity(CGFloat)
    case scale(CGFloat)
    
    init(_ slider: Slider, value: CGFloat) {
        switch slider {
        case .degrees: self = .degrees(value)
        case .density: self = .density(value)
        case .opacity: self = .opacity(value)
        case .scale: self = .scale(value)
        }
    }
    
    var value: CGFloat {
        switch self {
        case .degrees(let v): return v
        case .density(let v): return v
        case .opacity(let v): return v
        case .scale(let v): return v
        }
    }
}






