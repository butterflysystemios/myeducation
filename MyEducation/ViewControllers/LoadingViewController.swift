//
//  LoadingViewController.swift
//  MyEducation
//
//  Created by EK on 4/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import AWSS3
import MyEducationModel

final class LoadingViewController: UIViewController {
    
    var container: NSPersistentContainer!
    
    @IBOutlet weak var stackView: UIStackView!

    private let transferUtility = AWSS3TransferUtility.default()
    private let expression = AWSS3TransferUtilityDownloadExpression()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        expression.progressBlock = { [weak self] task, progress in
            guard let `self` = self else { return }
            DispatchQueue.main.async {
                print("Task key: \(task.key), progress: \(progress.fractionCompleted)")
                self.progressStates[task.key] = progress.fractionCompleted
            }
        }
    }
    
    private var progressStates: [String: Double] = [:]
    private let largeAssetsBucket = "education.largeassets"
    
    private func downloadVideo(name: String) {
        let downloadURL = URL.documents.appendingPathComponent(name)
        let task = transferUtility.download(
            to: downloadURL,
            bucket: largeAssetsBucket,
            key: name,
            expression: expression) { (task, url, data, error) in
        }
        task.continueWith { task -> Any? in
            if let result = task.result {
                print(result)
            }
            return nil
        }
    }
}
