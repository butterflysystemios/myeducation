//
//  NotificationDescriptor.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//


import Foundation

public struct NotificationDescriptor<A> {
    let name: Notification.Name
    let convert: (Notification) -> A
}

public class ObserverToken {
    let token: NSObjectProtocol
    let center: NotificationCenter
    init(token: NSObjectProtocol, center: NotificationCenter) {
        self.token = token
        self.center = center
    }
    
    deinit {
        center.removeObserver(token)
    }
}

extension NotificationCenter {
    func addObserver<A>(descriptor: NotificationDescriptor<A>, object: Any? = nil, queue: OperationQueue? = nil, using block: @escaping (A) -> ()) -> ObserverToken {
        let token = addObserver(forName: descriptor.name, object: nil, queue: nil, using: { note in
            block(descriptor.convert(note))
        })
        return ObserverToken(token: token, center: self)
    }
}

