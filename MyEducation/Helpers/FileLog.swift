//
//  FileLog.swift
//  MySetup
//
//  Created by abc on 14/8/17.
//  Copyright © 2017 Butterfly Systems Pty Ltd. All rights reserved.
//

import Foundation

final class FileLog {
    
    private let kMaxNumberOfLogFiles = 10
    private let kMaxFileSize = 1000 * 1024 //<1MB
    
    private let fm = FileManager.default
    private var fileHandle: UnsafeMutablePointer<FILE>?
    
    public static let shared = FileLog()
    
    public lazy var filesDirectory: String = {
        var path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        if !self.fm.isWritableFile(atPath: path) {
            path = NSTemporaryDirectory()
        }
        return path
    }()
    
    public lazy var currentLogFile: String = {
        return self.pathToFile(number: 0)
    }()
    
    public lazy var previousLogFile: String = {
        return self.pathToFile(number: 1)
    }()
    
    private init() { }
    
    private func pathToFile(number: Int) -> String {
        return filesDirectory.appendingFormat("/console%.2d.log", number)
    }
    
    private func fileExceedsMaxFileSize(_ path: String) -> Bool
    {
        if fm.fileExists(atPath: path) {
            let fileSize = (try! fm.attributesOfItem(atPath: path)[FileAttributeKey.size] as! NSNumber).uint64Value
            return fileSize > UInt64(kMaxFileSize)
        }
        return false
    }
    
    private func rotateFiles()
    {
        //Iterate through all log files and rename each one with an index higher than before
        for i in 0 ..< kMaxNumberOfLogFiles {
            let path = pathToFile(number: kMaxNumberOfLogFiles - 1 - i)
            if fm.fileExists(atPath: path) {
                let newPath = pathToFile(number: kMaxNumberOfLogFiles - i)
                do {
                    //overwrite the new file by deleting it first
                    if fm.fileExists(atPath: newPath) {
                        try fm.removeItem(atPath: newPath)
                    }
                    try fm.moveItem(atPath: path, toPath: newPath)
                    
                    //finally delete the current file once it's been copied
                    if path == currentLogFile {
                        try fm.removeItem(atPath: path)
                    }
                } catch let error as NSError {
                    print("failed renaming file \"\(path)\":), \(error.description)")
                }
            }
        }
    }
    
    //MARK: Public Interface
    
    public func redirectConsoleToFile()
    {
        if fileExceedsMaxFileSize(currentLogFile) {
            rotateFiles()
        }
        
        // redirect stdout if a terminal is not attached
        if (isatty(STDOUT_FILENO) == 0) {
            fileHandle = freopen(currentLogFile, "a+", stdout)
        }
    }
    
    public func flushOutput() {
        fflush(__stdoutp)
        fflush(fileHandle)
    }
    
    public func deleteAllLogFiles() {
        for i in 0...kMaxNumberOfLogFiles {
            let path = pathToFile(number: i)
            if fm.fileExists(atPath: path) {
                do {
                    try fm.removeItem(atPath: path)
                } catch let error as NSError {
                    print("failed deleting file \"\(path)\":), \(error.description)")
                }
            }
        }
    }
}
