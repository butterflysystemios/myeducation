//
//  EpisodeSource.swift
//  MyEducation
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers
import MyEducationModel


enum EpisodeSource {
    case all
    case filtered(String?)
    case course(MyEducationModel.Course)
}

extension EpisodeSource {
    var predicate: NSPredicate {
        switch self {
        case .all:
            return NSPredicate(value: true)
        case .filtered(let string):
            print(string ?? "no search term")
            return EpisodeSource.all.predicate // add this
        case .course(let course):
            let truePredicate = NSPredicate(value: true)
            let coursesPredicate = NSPredicate(format: "ANY supplements = %@", argumentArray: [course])
            return NSCompoundPredicate(andPredicateWithSubpredicates: [truePredicate, coursesPredicate])
        }
    }
    
    var managedObject: NSManagedObject? {
        switch self {
        case .course(let c):
            return c
        default:
            return nil
        }
    }
    
    func prefetch(in context: NSManagedObjectContext) -> [MyEducationModel.Episode] {
        switch self {
        case .all:
            return MyEducationModel.Episode.fetch(in: context) { request in
                request.predicate = MyEducationModel.Episode.defaultPredicate
            }
        case .filtered(let searchTerm):
            print(searchTerm ?? "no search term")
            return MyEducationModel.Episode.fetch(in: context) { request in
                request.predicate = MyEducationModel.Episode.defaultPredicate
            }
        case .course(let course):
            print(course)
            return MyEducationModel.Episode.fetch(in: context) { request in
                request.predicate = MyEducationModel.Episode.defaultPredicate
            }
        }
    }
}

