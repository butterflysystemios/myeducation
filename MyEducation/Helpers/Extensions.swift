//
//  Extensions.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import AVFoundation

extension UIView {
    func addConstraintsToSizeToParent(spacing: CGFloat = 0) {
        guard let view = superview else { fatalError() }
        let top = topAnchor.constraint(equalTo: view.topAnchor)
        let bottom = bottomAnchor.constraint(equalTo: view.bottomAnchor)
        let left = leftAnchor.constraint(equalTo: view.leftAnchor)
        let right = rightAnchor.constraint(equalTo: view.rightAnchor)
        view.addConstraints([top,bottom,left,right])
        if spacing != 0 {
            top.constant = spacing
            left.constant = spacing
            right.constant = -spacing
            bottom.constant = -spacing
        }
    }
    
    public var debugBorder: UIColor? {
        get { return layer.borderColor.map { UIColor(cgColor: $0) } }
        set {
            layer.borderColor = newValue?.cgColor
            layer.borderWidth = newValue != nil ? 1 : 0
        }
    }
    
    public static func activateDebugBorders(_ views: [UIView]) {
        let colors: [UIColor] = [.magenta, .orange, .green, .blue, .red]
        for (view, color) in zip(views, colors.cycled()) {
            view.debugBorder = color
        }
    }
}

extension Sequence {
    public func failingFlatMap<T>(_ transform: (Self.Iterator.Element) throws -> T?) rethrows -> [T]? {
        var result: [T] = []
        for element in self {
            guard let transformed = try transform(element) else { return nil }
            result.append(transformed)
        }
        return result
    }
    
    /// Returns a sequence that repeatedly cycles through the elements of `self`.
    public func cycled() -> AnySequence<Iterator.Element> {
        return AnySequence { () -> AnyIterator<Iterator.Element> in
            var iterator = self.makeIterator()
            return AnyIterator {
                if let next = iterator.next() {
                    return next
                } else {
                    iterator = self.makeIterator()
                    return iterator.next()
                }
            }
        }
    }
}

extension UIView {
    public func constrainEqual(_ attribute: NSLayoutAttribute, to: AnyObject, multiplier: CGFloat = 1, constant: CGFloat = 0) {
        constrainEqual(attribute, to: to, attribute, multiplier: multiplier, constant: constant)
    }
    
    public func constrainEqual(_ attribute: NSLayoutAttribute, to: AnyObject, _ toAttribute: NSLayoutAttribute, multiplier: CGFloat = 1, constant: CGFloat = 0) {
        NSLayoutConstraint.activate([
            NSLayoutConstraint(item: self, attribute: attribute, relatedBy: .equal, toItem: to, attribute: toAttribute, multiplier: multiplier, constant: constant)
            ]
        )
    }
}

extension UIViewController {
    public var contents: UIViewController {
        if let navigation = self as? UINavigationController {
            return navigation.visibleViewController!
        } else {
            return self
        }
    }
}

extension NSLayoutAnchor {
    @objc func constrainEqual(_ anchor: NSLayoutAnchor<AnchorType>, constant: CGFloat = 0) {
        let constraint = self.constraint(equalTo: anchor, constant: constant)
        constraint.isActive = true
    }
    
    @objc func constrainGreaterThanOrEqual(_ anchor: NSLayoutAnchor<AnchorType>, constant: CGFloat = 0) {
        let constraint = self.constraint(greaterThanOrEqualTo: anchor, constant: constant)
        constraint.isActive = true
    }
    
    @objc func constrainLessThanOrEqual(_ anchor: NSLayoutAnchor<AnchorType>, constant: CGFloat = 0) {
        let constraint = self.constraint(lessThanOrEqualTo: anchor, constant: constant)
        constraint.isActive = true
    }
}

extension Comparable {
    func clamped(to: ClosedRange<Self>) -> Self {
        if self < to.lowerBound { return to.lowerBound }
        if self > to.upperBound { return to.upperBound }
        return self
    }
}


func time(name: StaticString = #function, line: Int = #line, _ f: () -> ()) {
    let startTime = DispatchTime.now()
    f()
    let endTime = DispatchTime.now()
    let diff = (endTime.uptimeNanoseconds - startTime.uptimeNanoseconds) / 1_000_000
    print("\(name) (line \(line)): \(diff)")
}


public extension AVMetadataItem {
    /// - parameter language: The default is "und" ("undefined"), which is the fallback value for all languages that don't have a specific value set.
    /// - note: An initializer would be better for this, but it's not possible to write a "factory" initializer that internally instantiates a AVMutableMetadataItem (cannot assign to self).
    static func item(identifier: String, value: String?, language: String = "und") -> AVMetadataItem {
        let item = AVMutableMetadataItem()
        item.identifier = AVMetadataIdentifier(identifier)
        item.value = value as NSString?
        item.extendedLanguageTag = language
        return item.copy() as! AVMetadataItem
    }
    
    static func item(identifier: String, image: UIImage?, language: String = "und") -> AVMetadataItem {
        let item = AVMutableMetadataItem()
        item.identifier = AVMetadataIdentifier(identifier)
        if let image = image {
            item.value = UIImagePNGRepresentation(image) as NSData?
        } else {
            item.value = nil
        }
        item.dataType = kCMMetadataBaseDataType_PNG as String
        item.extendedLanguageTag = language
        return item.copy() as! AVMetadataItem
    }
}

