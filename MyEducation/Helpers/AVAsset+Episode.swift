//
//  AVAsset+Episode.swift
//  MyEducation
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import MyEducationModel
import AVFoundation

extension MyEducationModel.Episode {
    var asset: AVAsset {
        return AVAsset(url: self.storedAt!)
    }
}

extension AVAsset {
    /// Attempts to generate a thumbnail image data and duration. By default, the snapshot is at 50% (ie duration / 2.0).
    ///
    /// - Returns: A tuple with optional thumbnail image data and the optional duration of the asset
    func generateMetaData(takingSnapshotAtAssetDurationDividedBy divisor: CMTimeValue = 2) -> (thumbnail: Data?, duration: Double?) {
        var meta: (thumbnail: Data?, duration: Double?) = (thumbnail: nil, duration: nil)
        let assetGenerator = AVAssetImageGenerator(asset: self)
        assetGenerator.appliesPreferredTrackTransform = true
        
        let assetDuration = Double(CMTimeGetSeconds(self.duration))
        if assetDuration > 0 {
            meta.duration = assetDuration
        }
        
        var time = self.duration
        time.value = time.value / divisor
        if let imageRef = try? assetGenerator.copyCGImage(at: time, actualTime: nil) {
            let thumbnailImage = UIImage(cgImage: imageRef)
            meta.thumbnail = UIImagePNGRepresentation(thumbnailImage)
        }
        
        return meta
    }
}
