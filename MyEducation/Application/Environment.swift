//
//  Environment.swift
//  MyEducation
//
//  Created by EK on 5/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation

public struct Environment {
    public static let current = Environment()

    public enum Mode: String {
        case debug = "Debug"
        case tfs = "TFS"
        case dev = "Dev"
        case production = "Production"
    }

    public var mode: Environment.Mode {
        let environment = Bundle.main.object(forInfoDictionaryKey: "BSYSEnvironment") as! String
        return Mode(rawValue: environment)!
    }
    
    public var groupID: String {
        switch mode {
        case .debug, .dev, .tfs:
            return "group.systems.butterfly.MyEducationModel" // todo - make this the same as production syntax
        case .production:
            return "group.enterprise.systems.butterfly.MyEducation"
        }
    }
    
    public struct Amazon {
        static let baseURL: URL = {
            return URL(string: "https://s3-ap-southeast-2.amazonaws.com")!
        }()
        
        static let accessKey: String = {
           return "AKIAI6Z2II6LYAD62XMQ"
        }()
        
        static let secretKey: String = {
            return "JS9uTUFkG5wnWkihOmaDHmwOy+5LP2HyyR2UwSTR"
        }()
    }
}

extension Environment: CustomStringConvertible {
    public var description: String {
        return "*****\nApplication Environment\nMode:\(mode.rawValue)\nAmazonEndpoint:\(Amazon.baseURL)\n*****"
    }
}


