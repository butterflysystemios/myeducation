//
//  Theme.swift
//  MyEducation
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit

enum Theme {
    case dark // standard
    case light
    
    var navigationBarBarTintColor: UIColor? {
        switch self {
        case .dark:
            return UIColor(red: 36/255, green: 43/255, blue: 50/255, alpha: 1.0)
        case .light:
            return nil
        }
    }
    
    var navigationBarTintColor: UIColor? {
        switch self {
        case .dark:
            return .white
        case .light:
            return nil
        }
    }
    
    var navigationBarIsTranslucent: Bool {
        switch self {
        case .dark: return false
        case .light: return true
        }
    }
    
    var titleAttributes: [NSAttributedStringKey: Any]? {
        switch self {
        case .dark:
            return [NSAttributedStringKey.foregroundColor: UIColor.white]
        case .light:
            return nil
        }
    }
}

