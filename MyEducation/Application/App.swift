//
//  App.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreData
import MyEducationModel

final class App {
    private let container: NSPersistentContainer
    let navigationDelegate = EpisodesNavigationDelegate()
    private let screens: Screens
    private let rootTabController = UITabBarController()
    
    init(_ application: UIApplication, container: NSPersistentContainer, window: UIWindow) {
        self.container = container
        self.screens = Screens(container)
        rootTabController.tabBar.isHidden = true
        window.rootViewController = rootTabController
        updateCourses()
        rootTabController.viewControllers = [coverFlowTab(), videosTab()]
        setup()
    }
    
    private func coverFlowTab() -> UIViewController {
        let nav = screens.coverFlowTab()
        let courseContainer = screens.courseListingContainerViewController { [unowned self, unowned nav] episode in
            guard let _ = episode.storedAt else {
                let alertController = UIAlertController(title: "Importing Episode", message: "This video is not downloaded, please try playing again later.", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
                alertController.addAction(okAction)
                nav.visibleViewController?.present(alertController, animated: true, completion: nil)
                return
            }
            let videoPlayer = self.screens.video(episode)
            videoPlayer.didPlayToEnd = { [unowned nav] in
                nav.popViewController(animated: true)
            }
            nav.show(videoPlayer, sender: self)
        }
        nav.delegate = navigationDelegate
        nav.viewControllers = [courseContainer]
        return nav
    }
    
    private func videosTab() -> UIViewController {
        let nav = screens.videosTab()
        return nav
    }

    func receivedMemoryWarning() {
        container.viewContext.refreshAllObjects()
    }
    
    private func updateCourses() {
        
    }
    
    private func setup() {
        // additional setup & configuration
    }
}

extension App: SyncCoordinatorDelegate {
    func remoteDidUpdateProgress(_ remote: VideoRemote, importDescription: RemoteEpisode, progress: Progress) {
        print("\(importDescription.title) download fraction completed: \(progress.fractionCompleted)")
    }
}


final class EpisodesNavigationDelegate: NSObject, UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        //let isRootVC = navigationController.viewControllers.first == viewController
        //navigationController.hidesBarsOnTap = !isRootVC
        //navigationController.setNavigationBarHidden(!isRootVC, animated: animated)
        navigationController.navigationBar.prefersLargeTitles = false
    }
}
