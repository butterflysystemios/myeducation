//
//  Screens.swift
//  MyEducation
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit
import CoreData
import MyEducationModel

public final class Screens {
    fileprivate let container: NSPersistentContainer
    fileprivate let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    public init(_ container: NSPersistentContainer) {
        self.container = container
    }
    
    public func video(_ episode: Episode) -> VideoPlayerViewController {
        let vc = VideoPlayerViewController(episode)
        vc.title = episode.title
        return vc
    }
    
    public func courseListingContainerViewController(title: String = "Courses", didSelect: @escaping (Episode) -> () = { _ in }) -> UIViewController {
        let courseContainer = storyboard.instantiateViewController(withIdentifier: "CourseListingContainerViewController") as! CourseListingContainerViewController
        courseContainer.context = container.viewContext
        courseContainer.title = title
        courseContainer.didSelectEpisode = didSelect
        return courseContainer
    }
    
    public func coverFlowTab() -> UINavigationController {
        let nav = UINavigationController()
        nav.applyTheme()
        return nav
    }
    
    // OLD
    
    public func videosTab(title: String = "MyEducation") -> UINavigationController {
        let nav = UINavigationController()
        nav.applyTheme()
        nav.title = title
        return nav
    }
    
    public func allCourses(didSelect: @escaping (Course) -> () = { _ in }) -> UIViewController {
        let coverFlow = storyboard.instantiateViewController(withIdentifier: "CoverFlowViewController") as! CoverFlowViewController
        coverFlow.context = container.viewContext
        coverFlow.didSelect = didSelect
        return coverFlow
    }
}

extension UINavigationController {
    fileprivate func applyTheme(_ theme: Theme = .dark) {
        navigationBar.barTintColor = theme.navigationBarBarTintColor
        navigationBar.tintColor = theme.navigationBarTintColor
        navigationBar.isTranslucent = theme.navigationBarIsTranslucent
        navigationBar.titleTextAttributes = theme.titleAttributes
        navigationBar.largeTitleTextAttributes = theme.titleAttributes
    }
}


