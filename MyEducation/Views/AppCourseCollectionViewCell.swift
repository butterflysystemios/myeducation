//
//  AppCourseCollectionViewCell.swift
//  MyEducation
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit

final class AppCourseCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var coverImageView: UIImageView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
        coverImageView?.layer.cornerRadius = 8
        coverImageView?.layer.masksToBounds = true
    }
}

