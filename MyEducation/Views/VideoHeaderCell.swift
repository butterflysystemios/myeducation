//
//  VideoHeaderCell.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit

final class VideoHeaderCell: UICollectionReusableView {
    // MARK: - Effects
    private lazy var blurEffect: UIBlurEffect = {
        return UIBlurEffect(style: .light)
    }()
    
    private lazy var vibrancyEffect: UIVibrancyEffect = {
        return UIVibrancyEffect(blurEffect: self.blurEffect)
    }()
    
    // MARK: - Effect Views
    private lazy var blurredView: UIVisualEffectView = {
        let bv = UIVisualEffectView(effect: self.blurEffect)
        return bv
    }()
    
    private lazy var vibrancyView: UIVisualEffectView = {
        let vv = UIVisualEffectView(effect: self.vibrancyEffect)
        return vv
    }()

    let headerLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        headerLabel.font = UIFont.preferredFont(forTextStyle: .title1, compatibleWith: traitCollection)
        headerLabel.textColor = UIColor.white
        headerLabel.textAlignment = .left
        
        blurredView.translatesAutoresizingMaskIntoConstraints = false
        vibrancyView.translatesAutoresizingMaskIntoConstraints = false
        headerLabel.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(blurredView)
        blurredView.contentView.addSubview(vibrancyView)
        vibrancyView.contentView.addSubview(headerLabel)
        
        vibrancyView.addConstraintsToSizeToParent(spacing: 10)
        headerLabel.addConstraintsToSizeToParent()
        blurredView.addConstraintsToSizeToParent()
    }
}
