//
//  EpisodeCollectionViewCell.swift
//  MyEducation
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import UIKit

final class EpisodeCollectionViewCell : UICollectionViewCell {
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
