//
//  VideoImportDescription.swift
//  MyEducation
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import Foundation
import MyEducationModel
import CoreData
import CoreDataHelpers

struct RemoteEpisode: Codable {
    let remoteIdentifier: UUID
    let remotePath: String
    
    let title: String
    let part: Int
    let transcript: String?
    
    let createdAt: Date
    let updatedAt: Date
    let videoUpdatedAt: Date
    
    let courseCategories: [Int]
    let additionalTags: [String]
    
    
    enum CodingKeys: String, CodingKey {
        case remoteIdentifier = "id"
        case remotePath = "path"
        case title
        case part
        case transcript
        case createdAt = "release_date"
        case updatedAt = "last_updated"
        case courseCategories = "course_categories"
        case additionalTags = "tags"
        case videoUpdatedAt = "video_updated"
    }
}

