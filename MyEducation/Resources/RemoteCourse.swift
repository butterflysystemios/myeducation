//
//  RemoteCourse.swift
//  MyEducationMacOS
//
//  Created by eeekari on 25/3/19.
//  Copyright © 2019 systems.butterfly.MyEducationMacOS. All rights reserved.
//

import Foundation

struct RemoteCourse: Codable {
    let remoteIdentifier: UUID
    let name: String
    let category: Int
    let updatedAt: Date
    let createdAt: Date
    
    enum CodingKeys: String, CodingKey {
        case remoteIdentifier = "id"
        case name
        case category
        case updatedAt = "last_updated"
        case createdAt = "created_at"
    }
}
