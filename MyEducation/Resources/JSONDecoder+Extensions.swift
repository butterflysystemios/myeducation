//
//  JSONDecoder+Extensions.swift
//  MyEducationMacOS
//
//  Created by eeekari on 25/3/19.
//  Copyright © 2019 systems.butterfly.MyEducationMacOS. All rights reserved.
//

import Foundation

extension JSONDecoder {
    static let `default`: JSONDecoder = {
        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .secondsSince1970
        return decoder
    }()
}

extension JSONEncoder {
    static let `default`: JSONEncoder = {
        let decoder = JSONEncoder()
        decoder.outputFormatting = [.prettyPrinted, .sortedKeys]
        decoder.dateEncodingStrategy = .secondsSince1970
        return decoder
    }()
}
