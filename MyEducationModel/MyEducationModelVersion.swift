//
//  MyEducationModelVersion.swift
//  MyEducationModel
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreDataHelpers

enum MyEducationModelVersion: String {
    case version1 = "MyEducationModel"
}


extension MyEducationModelVersion: ModelVersion {
    static var all: [MyEducationModelVersion] {
         return [.version1]
    }
    static var current: MyEducationModelVersion {
        return .version1
    }
    var name: String { return rawValue }
    var modelBundle: Bundle { return Bundle(for: TargetBundle.self) }
    var modelDirectoryName: String { return "MyEducationModel.momd" }
}
