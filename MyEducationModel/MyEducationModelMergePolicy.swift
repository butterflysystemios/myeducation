//
//  MyEducationModelMergePolicy.swift
//  MyEducationModel
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers


public class MyEducationMergePolicy: NSMergePolicy {
    public enum MergeMode {
        case remote
        case local
        
        fileprivate var mergeType: NSMergePolicyType {
            switch self {
            case .remote: return .mergeByPropertyObjectTrumpMergePolicyType
            case .local: return .mergeByPropertyStoreTrumpMergePolicyType
            }
        }
    }
    
    required public init(mode: MergeMode) {
        super.init(merge: mode.mergeType)
    }
}
