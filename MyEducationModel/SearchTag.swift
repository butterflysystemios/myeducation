//
//  SearchTag.swift
//  MyEducationModel
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers

public final class SearchTag: NSManagedObject {
    @NSManaged public fileprivate(set) var term: String
    @NSManaged public fileprivate(set) var listings: Set<Episode>?
    
    internal static func findOrCreateTags(in context: NSManagedObjectContext, withTags searchTags: [String]) -> Set<SearchTag> {
        var searchSet: [SearchTag] = []
        let existingRecords = { () -> [String: SearchTag] in
            let tags = SearchTag.fetch(in: context, configurationBlock: { request in
                request.predicate = NSPredicate(format: "%K in %@", #keyPath(SearchTag.term), searchTags)
                request.returnsObjectsAsFaults = false
            })
            var result: [String: SearchTag] = [:]
            for tag in tags {
                result[tag.term] = tag
            }
            return result
        }()
        for searchTerm in searchTags {
            if let existingSearchTag = existingRecords[searchTerm] {
                searchSet.append(existingSearchTag)
            } else {
                let newTag: SearchTag = context.insertObject()
                newTag.term = searchTerm
                searchSet.append(newTag)
            }
        }
        return Set(searchSet)
    }
}

extension SearchTag: Managed {
    public static var entityName: String {
        return "SearchTag"
    }
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        let sd1 = NSSortDescriptor(key: "term", ascending: true)
        return [sd1]
    }
}
