//
//  MyEducationStack.swift
//  MyEducationModel
//
//  Created by EK on 1/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers

private let storeURL: URL = URL.group.appendingPathComponent("shared.education.v2")
private let container: NSPersistentContainer = {
    let container = NSPersistentContainer(name: "MyEducationModel", managedObjectModel: MyEducationModelVersion.current.managedObjectModel())
    let storeDescription = NSPersistentStoreDescription(url: storeURL)
    storeDescription.shouldMigrateStoreAutomatically = true
    storeDescription.shouldInferMappingModelAutomatically = true
    container.persistentStoreDescriptions = [storeDescription]
    return container
}()


public func createSharedContainer(migrating: Bool = false, progress: Progress? = nil, completion: @escaping (NSPersistentContainer) -> ()) {
    debugLog("Loading shared model from: \(storeURL.description)")
    container.loadPersistentStores { _, error in
        if error == nil {
            container.viewContext.mergePolicy = MyEducationMergePolicy(mode: .local)
            DispatchQueue.main.async { completion(container) }
        } else {
            guard !migrating else { fatalError("was unable to migrate store") }
            DispatchQueue.global(qos: .userInitiated).async {
                migrateStore(from: storeURL, to: storeURL, targetVersion: MyEducationModelVersion.current, deleteSource: true, progress: progress)
                createSharedContainer(migrating: true, progress: progress,
                                     completion: completion)
            }
        }
    }
}


internal func debugLog(_ any: Any?) {
    if let message = any {
        print("\n***** \nMyEducationModel debug logger logging to console: \n\(message)\n*****\n")
    }
}


extension URL {
    public static var temporary: URL {
        return URL(fileURLWithPath:NSTemporaryDirectory(), isDirectory: true).appendingPathComponent(UUID().uuidString)
    }
    
    public static var documents: URL {
        return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
    }
    
    public static var group: URL! {
        return FileManager.default.containerURL(forSecurityApplicationGroupIdentifier: Environment.current.groupID)
    }
}
