//
//  Episode.swift
//  MyEducationModel
//
//  Created by EK on 7/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers


public final class Episode: NSManagedObject {
    @NSManaged public fileprivate(set) var remoteIdentifier: UUID
    @NSManaged public fileprivate(set) var remotePath: String
    @NSManaged public fileprivate(set) var storedAt: URL?
    @NSManaged public fileprivate(set) var title: String
    @NSManaged public fileprivate(set) var transcript: String?
    @NSManaged public fileprivate(set) var createdAt: Date
    @NSManaged public fileprivate(set) var updatedAt: Date
    @NSManaged public fileprivate(set) var videoUpdatedAt: Date
    @NSManaged fileprivate var primitivePart: NSNumber
    
    @NSManaged fileprivate var primitiveDuration: NSNumber
    
    public var duration: Measurement<UnitDuration> {
        get {
            willAccessValue(forKey: "duration")
            let dur = primitiveDuration.doubleValue
            didAccessValue(forKey: "duration")
            return Measurement(value: dur, unit: .seconds)
        }
        set {
            willChangeValue(forKey: "duration")
            primitiveDuration = newValue.value as NSNumber
            didChangeValue(forKey: "duration")
        }
    }
    
    @NSManaged public var thumbnailData: Data?
    
    public fileprivate(set) var part: Int {
        get {
            willAccessValue(forKey: "part")
            let p = primitivePart as! Int
            didAccessValue(forKey: "part")
            return p
        }
        set {
            willChangeValue(forKey: "part")
            primitivePart = newValue as NSNumber
            didChangeValue(forKey: "part")
        }
    }
    
    @NSManaged public fileprivate(set) var supplements: Set<Course>
    @NSManaged public fileprivate(set) var tags: Set<SearchTag>
    
    
    public func reconfigureEpisodeInfo(updatedAt: Date, title: String, part: Int, transcript: String?, courseIdentifiers: [Int], searchTags: [String], in context: NSManagedObjectContext) {
        if self.updatedAt != updatedAt {
            print("Updated At value has changed. Reconfiguring episode info for title: \(title)")
            self.updatedAt = updatedAt
            self.title = title
            self.part = part
            self.transcript = transcript
            self.supplements = Course.courses(in: context, matching: courseIdentifiers)
            self.tags = SearchTag.findOrCreateTags(in: context, withTags: searchTags)
        }
    }
    
    public func saveVideoFileAt(url: URL) {
        if storedAt == nil || url != storedAt {
            self.storedAt = url
        }
    }
    
    public static func findOrCreateEpisode(in context: NSManagedObjectContext, remoteIdentifier: UUID, remotePath: String, title: String, transcript: String?, createdAt: Date, updatedAt: Date, videoUpdatedAt: Date, part: Int, searchTags: [String], courseIdentifiers: [Int]) -> Episode {
        
        let ep = Episode.findOrCreate(in: context, matching: NSPredicate(format: "%K == %@", argumentArray: [#keyPath(Episode.remoteIdentifier), remoteIdentifier]), configure: {_ in })
        ep.remoteIdentifier = remoteIdentifier
        ep.remotePath = remotePath
        ep.title = title
        ep.transcript = transcript
        ep.createdAt = createdAt
        ep.updatedAt = updatedAt
        ep.part = part
        ep.videoUpdatedAt = videoUpdatedAt
        ep.tags = SearchTag.findOrCreateTags(in: context, withTags: searchTags)
        ep.supplements = Course.courses(in: context, matching: courseIdentifiers)
        return ep
    }
    
    
    public static func insertOrUpdateEpisode(in context: NSManagedObjectContext, storedAt storageURL: URL?, remoteIdentifier: UUID, remotePath: String, title: String, transcript: String?, createdAt: Date, updatedAt: Date, videoUpdatedAt: Date, part: Int, searchTags: [String], courseIdentifiers: [Int]) -> Episode {
        
        let ep = Episode.findOrCreate(in: context, matching: NSPredicate(format: "%K == %@", argumentArray: [#keyPath(Episode.remoteIdentifier), remoteIdentifier]), configure: {_ in })
        ep.storedAt = storageURL
        ep.remoteIdentifier = remoteIdentifier
        ep.remotePath = remotePath
        ep.title = title
        ep.transcript = transcript
        ep.createdAt = createdAt
        ep.updatedAt = updatedAt
        ep.part = part
        ep.videoUpdatedAt = videoUpdatedAt
        ep.tags = SearchTag.findOrCreateTags(in: context, withTags: searchTags)
        ep.supplements = Course.courses(in: context, matching: courseIdentifiers)
        return ep
    }
}

extension Episode: Managed {
    public static var entityName: String {
        return "Episode"
    }
    
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        //let sd1 = //course description
        let sd2 = NSSortDescriptor(key: "part", ascending: true)
        return [sd2]
    }
}

