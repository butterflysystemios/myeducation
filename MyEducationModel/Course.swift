//
//  ApplicationCourse.swift
//  MyEducationModel
//
//  Created by EK on 6/12/17.
//  Copyright © 2017 ButterflySystems. All rights reserved.
//

import CoreData
import CoreDataHelpers


public final class Course: NSManagedObject {
    @NSManaged internal var primitiveCourseID: NSNumber
    @objc public fileprivate(set) var courseID: Int {
        get {
            willAccessValue(forKey: #keyPath(Course.courseID))
            let c = primitiveCourseID as! Int
            didAccessValue(forKey: #keyPath(Course.courseID))
            return c
        }
        set {
            willChangeValue(forKey: #keyPath(Course.courseID))
            primitiveCourseID = newValue as NSNumber
            didChangeValue(forKey: #keyPath(Course.courseID))
        }
    }
    
    @NSManaged public fileprivate(set) var coverImageData: Data?
    
    
    @NSManaged public fileprivate(set) var remoteIdentifier: UUID
    @NSManaged public fileprivate(set) var name: String
    @NSManaged public fileprivate(set) var lastUpdatedAt: Date
    
    @NSManaged public fileprivate(set) var createdAt: Date
    @NSManaged public fileprivate(set) var episodes: Set<Episode>
    
    @NSManaged fileprivate var primitiveLastUpdatedAt: Date
    
    public override func awakeFromInsert() {
        super.awakeFromInsert()
        primitiveLastUpdatedAt = Date()
    }
    
    internal static func courses(in context: NSManagedObjectContext, matching courseIDs: [Int]) -> Set<Course> {
        let predicate = NSPredicate(format: "%K in %@", "courseID", courseIDs)
        let results = Course.fetch(in: context) {
            $0.predicate = predicate
        }
        return Set(results)
    }
    
    
    /// Inserts a new course
    ///
    /// - Parameters:
    ///   - name: The name of the app course
    ///   - coverImageData: The cover image data of the app course
    ///   - remoteIdentifier: the remote identifier (if exists on backend)
    ///   - context: the context to insert in
    /// - Returns: A new app course
    public static func insertNewCourse(with courseID: Int, named name: String, coverImageData: Data?, remoteIdentifier: UUID, createdAt: Date, lastUpdatedAt: Date, in context: NSManagedObjectContext) -> Course {
        let appCourse: Course = context.insertObject()
        appCourse.courseID = courseID
        appCourse.name = name
        appCourse.createdAt = createdAt
        appCourse.coverImageData = coverImageData
        appCourse.remoteIdentifier = remoteIdentifier
        appCourse.lastUpdatedAt = lastUpdatedAt
        return appCourse
    }
    
    /// Finds or creates a course by name. If no course is found, one is created with the other arguments. Otherwise an unmodified course is returned.
    ///
    /// - Parameters:
    ///   - name: The name of the app course
    ///   - coverImageData: The Data to use for the cover view
    ///   - remoteIdentifier: The remoteIdentifier of the app.
    ///   - context: The context to search or insert in
    public static func findOrCreateCourse(with courseID: Int, name: String, coverImageData: Data?, remoteIdentifier: UUID, createdAt: Date, lastUpdatedAt: Date, in context: NSManagedObjectContext) -> Course {
        let courseIDPredicate = NSPredicate(format: "%K == %@", argumentArray: [#keyPath(Course.remoteIdentifier), remoteIdentifier])
        let course = Course.findOrCreate(in: context, matching: courseIDPredicate) { _ in }
    
        // always set these.
        course.remoteIdentifier = remoteIdentifier
        course.courseID = courseID
        course.name = name
        course.createdAt = createdAt
        course.lastUpdatedAt = lastUpdatedAt
        if course.coverImageData == nil || coverImageData != course.coverImageData {
            course.coverImageData = coverImageData
        }
        return course
    }
}

extension Course: Managed {
    public static var defaultSortDescriptors: [NSSortDescriptor] {
        let sd1 = NSSortDescriptor(key: "courseID", ascending: true)
        return [sd1]
    }
    
    public static var entityName: String {
        return "Course"
    }
}
