# README #

This README documents whatever steps are necessary to get your application up and running with this framework.

### What is this repository for? ###

* ButterflyOperations is a framework that leverages Foundation.Operation and Foundation.OperationQueue. It enables use of convenient operations.

* This is an adaptation of the sample code provided in the [Advanced NSOperations](https://developer.apple.com/videos/play/wwdc2015/226) session of WWDC 2015.

### How do I get set up? ###

* Add the project to your workspace, import as required.

### Contribution guidelines ###
* In an ideal world...
	* Write some tests
	* Review your code
	* Submit a pull request
	* Get another team member to look at it

### Who do I talk to? ###

* That nagging voice inside your head.