//
//  QualityOfService+.swift
//  ButterflyOperations
//
//  Created by EK on 15/9/17.
//  Copyright © 2017 Butterfly Systems. All rights reserved.
//

import Foundation

extension DispatchQoS.QoSClass {
    init(qos: QualityOfService) {
        switch qos {
        case .userInteractive:
            self = .userInteractive
        case .userInitiated:
            self = .userInitiated
        case .utility:
            self = .utility
        case .background:
            self = .background
        case .default:
            self = .default
        }
    }
}
