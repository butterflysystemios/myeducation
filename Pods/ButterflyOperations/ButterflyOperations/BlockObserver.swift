/*
Copyright (C) 2015 Apple Inc. All Rights Reserved.
See LICENSE.txt for this sample’s licensing information

Abstract:
This file shows how to implement the OperationObserver protocol.
*/
import Foundation


/**
 The `BlockObserver` is a way to attach arbitrary blocks to significant events
 in an `Operation`'s lifecycle.
 */
public class BlockObserver: OperationObserver {
    // MARK: Properties
    
    
    fileprivate let startHandler: ((Operation) -> ())?
    fileprivate let cancelHandler: ((Operation) -> ())?
    fileprivate let produceHandler: ((Operation, Foundation.Operation) -> ())?
    fileprivate let finishHandler: ((Operation, [NSError]) -> ())?
    
    public init(startHandler: ((Operation) -> ())? = nil, cancelHandler: ((Operation) -> ())? = nil, produceHandler: ((Operation, Foundation.Operation) -> ())? = nil, finishHandler: ((Operation, [NSError]) -> ())? = nil) {
        self.startHandler = startHandler
        self.cancelHandler = cancelHandler
        self.produceHandler = produceHandler
        self.finishHandler = finishHandler
    }
    
    // MARK: OperationObserver
    
    public func operationDidStart(_ operation: Operation) {
        startHandler?(operation)
    }
    
    public func operationDidCancel(_ operation: Operation) {
        cancelHandler?(operation)
    }
    
    public func operation(_ operation: Operation, didProduceOperation newOperation: Foundation.Operation) {
        produceHandler?(operation, newOperation)
    }
    
    public func operationDidFinish(_ operation: Operation, errors: [NSError]) {
        finishHandler?(operation, errors)
    }
}

