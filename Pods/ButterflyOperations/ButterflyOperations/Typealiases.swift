//
//  Typealiases.swift
//  ButterflyOperations
//
//  Created by EK on 15/9/17.
//  Copyright © 2017 Butterfly Systems. All rights reserved.
//


public typealias BSOperation = Operation
public typealias BSOperationQueue = OperationQueue
public typealias BSOperationQueueDelegate = OperationQueueDelegate
public typealias BSBlockOperation = BlockOperation
