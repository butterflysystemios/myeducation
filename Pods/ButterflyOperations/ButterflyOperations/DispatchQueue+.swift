//
//  DispatchQueue+.swift
//  ButterflyOperations
//
//  Created by EK on 15/9/17.
//  Copyright © 2017 Butterfly Systems. All rights reserved.
//

import Foundation

extension DispatchQueue {
    class func global(qos: QualityOfService) -> DispatchQueue {
        return global(qos: .init(qos: qos))
    }
}
